# -*- encoding: utf-8 -*-
import sys
import urllib2
import urllib
import cookielib
import re

import pymongo

from urlparse import urljoin

from BeautifulSoup import BeautifulSoup


BASE_URL = 'http://level2.ciklum.net/'
LOGIN = ''
PASSWORD = ''
LOGIN_URL = 'http://level2.ciklum.net/index.php'
EMPLOYEES_LIST_URL = 'http://level2.ciklum.net/index.php?option=com_ciklum&task=countryTeam&Itemid=546'


client = pymongo.MongoClient('localhost', 27017)
db = client.level2


def auth(username, password):
    print 'Authentication...'
    jar = cookielib.FileCookieJar("cookies")
    data=urllib.urlencode({'Submit': 'Login',
                           'lang': 'english',
                           'message': '0',
                           'op2': 'login',
                           'option': 'login',
                           'return': '/index.php?option=com_frontpage&Itemid=1',
                           'username': username,
                           'passwd': password,
                           'validate': '1'})
    _opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(jar))
    # first auth with encoded data, in this step cookies will be received
    request = urllib2.Request(LOGIN_URL, data)
    response = _opener.open(request)
    data = response.read()
    if len(re.findall('You are not authorized to view this resource', data)) == 0:
        print 'Authenticated.'
        urllib2.install_opener(_opener)
    else:
        print data
        print '*'*100
        print 'NOT AUTHENTICATED!'

        sys.exit(0)


def get_employees_list_page():
    data = urllib2.urlopen(EMPLOYEES_LIST_URL)
    return data.read()


def parse_employees_list():
    data = get_employees_list_page()
    soup = BeautifulSoup(data)
    table = soup.find('table', {'id': 'teamList'})

    a_all = table.findAll('a')
    urls = []

    for a in a_all:
        if re.findall('showPerson', str(a)):
            urls.append(a.get('href'))

    return urls


def get_employee_page(url):
    print url
    page = urllib2.urlopen(url)
    return page.read()


def parse_employee_page(url):
    page = get_employee_page(url)
    soup = BeautifulSoup(page)
    #personalInfo
    table = soup.find('table', {'class': 'personalInfo'})
    tr_all = table.findAll('tr')
    name = None
    position = None
    phone = None
    last_i = len(tr_all) - 1
    for i, tr in enumerate(tr_all):
        tds = tr.findAll('td')
        if tds[0].string == 'Person Name':
            name = tds[1].b.string
        elif tds[0].string == 'Position':
            position = tds[1].string
        elif tds[0].string == 'Mobile':
            phone = tds[1].string

        if i == last_i:
            doc = {'name': name,
                   'position': position,
                   'phone': phone}
            db.employees.insert(doc)
    return



def parse_all():
    profile_links = parse_employees_list()
    for i, url in enumerate(profile_links):
        print '%s...' % i
        parse_employee_page(urljoin(BASE_URL, url))
    print 'Done, exit...'



def main():
    print 'Start app...'
    auth(LOGIN, PASSWORD)
    parse_all()


if __name__ == '__main__':
    main()